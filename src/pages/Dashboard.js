import React, { Component } from "react";
import isLoggedInLocally, { logOut } from "../components/TokenCheck";
import { Redirect, NavLink } from "react-router-dom";
import { makeAPICall } from "../data-access/UserDataAccess";

import { ReactComponent as Edit } from "../images/edit.svg";
import { ReactComponent as Check } from "../images/check.svg";
import { ReactComponent as Menu } from "../images/list.svg";
import { ReactComponent as Gear } from "../images/settings.svg";
import { ReactComponent as Plus } from "../images/plus.svg";
import {
    GET_FOODS,
    REMOVE_FOOD,
    ADD_FOOD,
    UPDATE_FOOD,
    DATE_FORMAT,
} from "../components/Constants";
import EditPanel from "../components/EditPanel";
import moment from "moment-timezone";

class Dashboard extends Component {
    constructor(props) {
        super(props);

        console.log(new Date().getTimezoneOffset());
        console.log(moment.tz.guess(true));
        console.log(moment().format(DATE_FORMAT));

        this.state = {
            foodList: [],
            errorRetrievingFoods: false,
            showEditPanel: false,
            editFood: null,
        };

        this.onAddClick = this.onAddClick.bind(this);
        this.onCheckClick = this.onCheckClick.bind(this);
        this.onEditClick = this.onEditClick.bind(this);
        this.handleEditPanelChange = this.handleEditPanelChange.bind(this);
        this.handleCancelEditPanel = this.handleCancelEditPanel.bind(this);
    }

    onAddClick() {
        this.setState({ showEditPanel: true, editFood: null });
    }

    /**
     * Check mark next to food clicked
     *
     * @param {String} id the food id
     */
    async onCheckClick(id) {
        // make api call
        let response = await makeAPICall(REMOVE_FOOD, { food_id: id });

        if (response.success != null && !response.success) {
            if (
                response.message ===
                "Given token does not match the user's login token."
            ) {
                logOut();
            }
            this.setState({ errorRetrievingFoods: true });
        } else {
            // update foods
            this.updateFoodList();
        }
    }

    async updateFoodList() {
        let response = await makeAPICall(GET_FOODS);

        if (response.success != null && !response.success) {
            if (
                response.message ===
                    "Given token does not match the user's login token." ||
                response.message === "User with the given id does not exist."
            ) {
                logOut();
            }
            this.setState({ errorRetrievingFoods: true });
        } else {
            let foodList = response.data.user_foods;

            console.log(foodList);

            let sortedList = foodList.sort((a, b) => {
                if (a.expDate === b.expDate) {
                    let nameA = a.name.toLowerCase();
                    let nameB = b.name.toLowerCase();

                    if (nameA < nameB) {
                        return -1;
                    } else if (nameA > nameB) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
                return new Date(a.expDate) - new Date(b.expDate);
            });
            this.setState({ foodList: sortedList });
        }
    }

    /**
     * Event for when edit button is clicked
     *
     * @param {String} id the food's id
     */
    onEditClick(id) {
        console.log("clicked");
        // find the food with the id
        console.log(id);

        let editFood = this.state.foodList.find((food) => food._id === id);

        this.setState({ showEditPanel: true, editFood: editFood });
    }

    /**
     * Handles submitting edit panel values
     *
     * @param {Food} newFood the food object
     * @param {Boolean} foodExists if food exists, user is editing;
     * otherwise user is adding a new food
     */
    async handleEditPanelChange(newFood, foodExists) {
        console.log("handlechange");
        console.log(newFood);

        // check if it's an add vs edit, and make the right api call whoops
        // make the api call to add it
        let response;
        if (foodExists) {
            response = await makeAPICall(UPDATE_FOOD, {
                food: newFood,
                food_id: this.state.editFood._id,
            });
        } else {
            response = await makeAPICall(ADD_FOOD, { food: newFood });
        }
        console.log(response);

        if (response && response.data && response.data.success) {
            this.setState({ showEditPanel: false });
            this.updateFoodList();
        }
    }

    /**
     * Called when cancel is pressed or escape is pressed when the edit panel
     * is open
     */
    async handleCancelEditPanel() {
        this.setState({
            showEditPanel: false,
            editFood: null,
        });
    }

    async componentDidMount() {
        this.updateFoodList();
    }

    render() {
        if (!isLoggedInLocally()) {
            return <Redirect push to="/login" />;
        }

        let foodListElements = this.state.foodList.map((f) => (
            <FoodElement
                name={f.name}
                expDate={f.expDate}
                key={f._id}
                food_id={f._id}
                onCheckClick={this.onCheckClick}
                onEditClick={() => {
                    this.onEditClick(f._id);
                }}
            />
        ));

        return (
            <div className="dashboard-wrapper">
                <div className="title-header">Dashboard</div>
                <div id="dashboard-list-wrapper">
                    {this.state.errorRetrievingFoods && (
                        <div className="error-message">
                            Error loading your foods
                            <br />
                            <br />
                            Try refreshing the page
                        </div>
                    )}
                    {foodListElements}
                    <div id="desktop-add-button-wrapper">
                        <div
                            id="desktop-add-button"
                            className="check-button"
                            onClick={this.onAddClick}
                        >
                            <Plus />
                        </div>
                    </div>
                </div>
                <div id="mobile-nav-bar">
                    <div id="menu-button">
                        <Menu />
                    </div>
                    <div id="preferences-button">
                        <NavLink to="/preferences">
                            <Gear />
                        </NavLink>
                    </div>
                    <div id="add-button" onClick={this.onAddClick}>
                        <Plus />
                    </div>
                </div>
                {this.state.showEditPanel && (
                    <EditPanel
                        onChange={this.handleEditPanelChange}
                        onCancel={this.handleCancelEditPanel}
                        editFood={this.state.editFood}
                    />
                )}
            </div>
        );
    }
}

class FoodElement extends Component {
    constructor(props) {
        super(props);

        this.onCheckClick = this.onCheckClick.bind(this);
        this.onEditClick = this.onEditClick.bind(this);
    }

    onCheckClick() {
        this.props.onCheckClick(this.props.food_id);
    }

    onEditClick() {
        this.props.onEditClick(this.props.food_id);
    }

    dateToString(date) {
        let newDate = new Date(this.props.expDate);
        return (
            newDate.getMonth() +
            1 +
            "/" +
            newDate.getDate() +
            "/" +
            newDate.getFullYear()
        );
    }

    render() {
        return (
            <div className="food-wrapper">
                <div className="food-date-buttons-wrapper">
                    <div className="food-exp-date">
                        {this.dateToString(this.props.expDate)}
                    </div>
                    <div className="food-buttons-wrapper">
                        <div
                            className="food-edit-button check-button"
                            title="edit"
                            onClick={this.onEditClick}
                        >
                            <Edit />
                        </div>
                        <div
                            className="food-check-button check-button"
                            title="eaten"
                            onClick={this.onCheckClick}
                        >
                            <Check />
                        </div>
                    </div>
                </div>
                <div className="food-name">{this.props.name}</div>
            </div>
        );
    }
}

export default Dashboard;
