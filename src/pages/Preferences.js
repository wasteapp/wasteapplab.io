import React, { Component } from "react";
import isLoggedInLocally from "../components/TokenCheck";
import { Redirect } from "react-router-dom";

import { GET_PREFS, UPDATE_PREFS } from "../components/Constants";

import { ReactComponent as Check } from "../images/check.svg";
import TimePicker from "../components/TimePicker";
import { makeAPICall } from "../data-access/UserDataAccess";
import DaysPicker from "../components/DaysPicker";
import InfoButton from "../components/InfoButton";

class Preferences extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            goBackToDashboard: false,
            errorLoadingPreferences: false,
        };

        this.handleBreakfastTimeChange = this.handleBreakfastTimeChange.bind(
            this
        );
        this.handleLunchTimeChange = this.handleLunchTimeChange.bind(this);
        this.handleDinnerTimeChange = this.handleDinnerTimeChange.bind(this);
        this.handleExpirationDaysChange = this.handleExpirationDaysChange.bind(
            this
        );
        this.handleExpirationDateEmailsChange = this.handleExpirationDateEmailsChange.bind(
            this
        );
        this.handlePreMealRemindersChange = this.handlePreMealRemindersChange.bind(
            this
        );
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleBreakfastTimeChange(newTime) {
        let preferences = this.state.preferences;
        preferences.mealTimes.breakfast = newTime;

        this.setState({ preferences: preferences });
    }

    handleLunchTimeChange(newTime) {
        let preferences = this.state.preferences;
        preferences.mealTimes.lunch = newTime;

        this.setState({ preferences: preferences });
    }

    handleDinnerTimeChange(newTime) {
        let preferences = this.state.preferences;
        preferences.mealTimes.dinner = newTime;

        this.setState({ preferences: preferences });
    }

    handleExpirationDaysChange(newDays) {
        let preferences = this.state.preferences;
        preferences.daysBeforeExpirationToNotify = newDays;

        this.setState({ preferences: preferences });
    }

    handleExpirationDateEmailsChange() {
        let preferences = this.state.preferences;
        preferences.emailPreferences.receiveExpDateReminders = !preferences
            .emailPreferences.receiveExpDateReminders;

        this.setState({ preferences: preferences });
    }

    handlePreMealRemindersChange() {
        let preferences = this.state.preferences;
        preferences.emailPreferences.receiveMealTimeReminders = !preferences
            .emailPreferences.receiveMealTimeReminders;

        this.setState({ preferences: preferences });
    }

    async handleSubmit() {
        // TODO show a loading symbol for saving

        let response = await makeAPICall(UPDATE_PREFS, {
            user_preferences: this.state.preferences,
        });

        if (response.success != null && !response.success) {
            console.log("Problem updating preferences.");
            // TODO show an error somewhere
        }

        this.setState({ goBackToDashboard: true });
    }

    async componentDidMount() {
        let response = await makeAPICall(GET_PREFS);

        if (response.success != null && !response.success) {
            if (
                response.message ===
                "Given token does not match the user's login token."
            ) {
                localStorage.clear();
            }
            this.setState({ errorLoadingPreferences: true });
        } else {
            this.setState({
                preferences: response.data.user_preferences,
            });

            // loading is done
            this.setState({
                loading: false,
            });
        }
    }

    render() {
        if (!isLoggedInLocally()) {
            return <Redirect push to="/login" />;
        }

        if (this.state.goBackToDashboard) {
            return <Redirect push to="/dashboard" />;
        }

        return (
            <div>
                <div className="title-header">Preferences</div>
                <div className="preferences-wrapper">
                    {this.state.errorLoadingPreferences && (
                        <div className="error-message">
                            Error loading your preferences
                            <br />
                            <br />
                            Try refreshing the page
                        </div>
                    )}
                    {!this.state.loading && (
                        <div className="preferences-list-wrapper">
                            <div className="single-preference-wrapper">
                                <div className="preference-title">
                                    <div>Meal Times</div>
                                    <InfoButton>
                                        When do you want email reminders?
                                        <br />
                                        <br />
                                        Expiration reminders are sent at
                                        breakfast!
                                    </InfoButton>
                                </div>
                                <div className="meal-time">
                                    <div>Breakfast</div>
                                    <TimePicker
                                        onChange={
                                            this.handleBreakfastTimeChange
                                        }
                                        time={
                                            this.state.preferences.mealTimes
                                                .breakfast
                                        }
                                    />
                                </div>
                                <div className="meal-time">
                                    <div>Lunch</div>
                                    <TimePicker
                                        onChange={this.handleLunchTimeChange}
                                        time={
                                            this.state.preferences.mealTimes
                                                .lunch
                                        }
                                    />
                                </div>
                                <div className="meal-time">
                                    <div>Dinner</div>
                                    <TimePicker
                                        onChange={this.handleDinnerTimeChange}
                                        time={
                                            this.state.preferences.mealTimes
                                                .dinner
                                        }
                                    />
                                </div>
                            </div>
                            <div className="single-preference-wrapper">
                                <div className="preference-title">
                                    <div>Email Preferences</div>
                                    <InfoButton>
                                        Which emails do you want to receive?
                                        <br />
                                        <br />
                                        You will only receive emails if there
                                        are any relevant foods for that meal.
                                    </InfoButton>
                                </div>
                                <div className="email-preference">
                                    Email me{" "}
                                    <DaysPicker
                                        days={
                                            this.state.preferences
                                                .daysBeforeExpirationToNotify
                                        }
                                        maxDays={7}
                                        onChange={
                                            this.handleExpirationDaysChange
                                        }
                                    />{" "}
                                    before food expires
                                </div>
                                <div className="email-preference">
                                    Expiration Date Emails
                                    <div
                                        className={
                                            "email-preference-check-button check-button " +
                                            (this.state.preferences
                                                .emailPreferences
                                                .receiveExpDateReminders
                                                ? "checked"
                                                : "")
                                        }
                                        onClick={
                                            this
                                                .handleExpirationDateEmailsChange
                                        }
                                    >
                                        <Check />
                                    </div>
                                </div>
                                <div className="email-preference">
                                    Pre Meal Reminders
                                    <div
                                        className={
                                            "email-preference-check-button check-button " +
                                            (this.state.preferences
                                                .emailPreferences
                                                .receiveMealTimeReminders
                                                ? "checked"
                                                : "")
                                        }
                                        onClick={
                                            this.handlePreMealRemindersChange
                                        }
                                    >
                                        <Check />
                                    </div>
                                </div>
                            </div>
                            <div id="preferences-buttons-wrapper">
                                <div
                                    className="theme-button"
                                    onClick={this.handleSubmit}
                                >
                                    Done
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default Preferences;
