import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { createUser } from "../data-access/UserDataAccess";
import { TOKEN_KEY, USER_ID_KEY } from "../components/Constants";
import isLoggedInLocally from "../components/TokenCheck";

class SignUp extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            key: "",
            errorMessage: "",
            loading: false,
        };

        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleKeyChange = this.handleKeyChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleFirstNameChange(event) {
        this.setState({ firstName: event.target.value });
    }

    handleLastNameChange(event) {
        this.setState({ lastName: event.target.value });
    }

    handleEmailChange(event) {
        this.setState({ email: event.target.value });
    }

    handleKeyChange(event) {
        this.setState({ key: event.target.value });
    }

    async handleSubmit(event) {
        event.preventDefault();

        // clears the message
        this.setState({ errorMessage: "" });

        // TODO actually use loading state to trigger animation
        // start the loading animation
        this.setState({ loading: true });

        console.log("about to make api call");

        // make the api call using a method
        let response = await createUser(
            this.state.firstName,
            this.state.lastName,
            this.state.email,
            this.state.key
        );

        console.log("api call is done");

        // set the error message
        if (response.success != null && !response.success) {
            if (this._isMounted) {
                this.setState({ errorMessage: response.message });
            }
        } else {
            // set the token in local storage
            localStorage.setItem(TOKEN_KEY, response.data.user_token);
            localStorage.setItem(USER_ID_KEY, response.data.user_id);

            // end the loading animation
            this.setState({ loading: false });

            console.log("localstorage is set");
        }
    }

    render() {
        if (isLoggedInLocally()) {
            return <Redirect push to="/dashboard" />;
        }

        return (
            <div className="signup-wrapper">
                <div className="waste-title">Waste</div>
                <div className="error-message">{this.state.errorMessage}</div>
                <div id="signup-title">Sign Up</div>
                <form id="signup-form" onSubmit={this.handleSubmit}>
                    <div id="signup-name-wrapper">
                        <input
                            autoFocus
                            id="first-name-input"
                            onChange={this.handleFirstNameChange}
                            placeholder="first name"
                            type="text"
                            value={this.state.firstName}
                        />
                        <input
                            id="last-name-input"
                            onChange={this.handleLastNameChange}
                            placeholder="last name"
                            type="text"
                            value={this.state.lastName}
                        />
                    </div>
                    <input
                        id="email-input"
                        onChange={this.handleEmailChange}
                        placeholder="email"
                        type="text"
                        value={this.state.email}
                    />
                    <input
                        id="key-input"
                        type="password"
                        placeholder="password"
                        value={this.state.key}
                        onChange={this.handleKeyChange}
                    />
                    <div id="signup-submit-wrapper">
                        <input
                            className="theme-button"
                            id="signup-submit"
                            type="submit"
                            value="Sign Up"
                        />
                    </div>
                </form>
            </div>
        );
    }
}

export default SignUp;
