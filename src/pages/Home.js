import React, { Component } from "react";
import HighlightLink from "../components/HighlightLink";
import AboutSection from "../components/AboutSection";
import Footer from "../components/Footer";

import { ReactComponent as ChevronDown } from "../images/chevron-down.svg";

class Home extends Component {
    constructor(props) {
        super(props);

        this.aboutRef = React.createRef();
        this.handleChevronClick = this.handleChevronClick.bind(this);
    }

    handleChevronClick() {
        this.aboutRef.current.scrollIntoView({
            behavior: "smooth",
            block: "start",
        });
    }

    render() {
        return (
            <div className="home-wrapper">
                <div id="home-top">
                    <div className="waste-title">Waste</div>
                    <div className="waste-title-description">
                        Receive email reminders when your food is about to
                        expire.
                    </div>

                    <HighlightLink id="login-button" link="/login">
                        Login
                    </HighlightLink>
                    <HighlightLink id="signup-button" link="/signup">
                        Sign Up
                    </HighlightLink>
                    <ChevronDown
                        id="chevron-down-button"
                        src={ChevronDown}
                        alt="chevron down"
                        onClick={this.handleChevronClick}
                    ></ChevronDown>
                </div>
                <div id="home-about" ref={this.aboutRef}>
                    <AboutSection title="Hold up! What IS this website?">
                        It's a leftovers and groceries reminder app!
                    </AboutSection>
                    <AboutSection title="What does it do?">
                        Simply write down a list of leftovers or groceries you
                        want to remember.
                        <br />
                        <br />
                        This app will send email reminders (and hopefully SMS
                        reminders eventually) every morning and before every
                        meal!
                        <br />
                        <br />
                        You can change your email settings in the preferences
                        though, so don't worry about getting spammed!
                    </AboutSection>
                    <AboutSection title="I still don't get it...">
                        Let's say you have these really tasty dried mango
                        strips, but SOMEONE keeps stealing them, so you hide
                        them in a drawer somewhere.
                        <br />
                        <br />
                        Before this app, you would forget about them and
                        something suspicious would start growing in the drawer.
                        <br />
                        <br />
                        Now, you'll receive a reminder before breakfast, for
                        example, that says "Hey don't forget to eat your mango
                        strips before they expire."
                        <br />
                        <br />
                        Also, 2 or 3 days before they expire, you'll receive
                        another email saying "Hey, your mango strips are going
                        to expire soon! Eat them or feed them to your
                        roommates."
                    </AboutSection>
                    <AboutSection title="Why though?">
                        Well, I had 5 roommates and it was hard to keep track of
                        what food and leftovers I did and didn’t have. I also
                        kept forgetting to finish certain leftovers or snacks
                        and I didn’t want to waste (roll credits) them. Thus,
                        the email reminders.
                    </AboutSection>
                    <AboutSection title="What technologies did you use?">
                        ReactJS for the frontend
                        <br />
                        <br />
                        MongoDB, Express, NodeJS for the backend
                        <br />
                        <br />
                        Chai and Mocha for API testing (that's right, I wrote
                        tests!)
                    </AboutSection>
                    <AboutSection title="Is it any good?">Yes</AboutSection>
                    <AboutSection title="Future plans?">
                        The original plan was to add SMS notifications because
                        then they wouldn't fill up your inbox. But! It costs
                        money and also people in other countries wouldn't be
                        able to get notifications, so email seemed like the way
                        to go.
                    </AboutSection>
                    <AboutSection title="There's so much writing here :(">
                        Sorry... I need to be thorough in case potential
                        employers see this! :O
                        <br />
                        <br />
                        (Please hire me)
                    </AboutSection>
                </div>
                <Footer />
            </div>
        );
    }
}

export default Home;
