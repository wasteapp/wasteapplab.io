import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { loginUser } from "../data-access/UserDataAccess";
import { TOKEN_KEY, USER_ID_KEY } from "../components/Constants";
import isLoggedInLocally from "../components/TokenCheck";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            key: "",
            errorMessage: "",
            loading: false,
        };

        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleKeyChange = this.handleKeyChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleEmailChange(event) {
        this.setState({ email: event.target.value });
    }

    handleKeyChange(event) {
        this.setState({ key: event.target.value });
    }

    async handleSubmit(event) {
        event.preventDefault();

        let response = await loginUser(this.state.email, this.state.key);

        // set the error message
        if (response.success != null && !response.success) {
            this.setState({ errorMessage: response.message });
        } else {
            // set the token in local storage
            localStorage.setItem(TOKEN_KEY, response.data.user_token);
            localStorage.setItem(USER_ID_KEY, response.data.user_id);

            // redirect to dashboard
            this.props.history.push("/dashboard");
        }
    }

    render() {
        if (isLoggedInLocally()) {
            return <Redirect push to="/dashboard" />;
        }

        return (
            <div className="login-wrapper">
                <div className="waste-title">Waste</div>
                <div className="error-message">{this.state.errorMessage}</div>
                <div id="login-title">Login</div>
                <form id="login-form" onSubmit={this.handleSubmit}>
                    <input
                        autoFocus
                        id="email-input"
                        onChange={this.handleEmailChange}
                        placeholder="email"
                        type="text"
                        value={this.state.email}
                    />
                    <input
                        id="key-input"
                        type="password"
                        placeholder="password"
                        value={this.state.key}
                        onChange={this.handleKeyChange}
                    />
                    <div id="login-submit-wrapper">
                        <input
                            className="theme-button"
                            id="login-submit"
                            type="submit"
                            value="Login"
                        />
                    </div>
                </form>
            </div>
        );
    }
}

export default Login;
