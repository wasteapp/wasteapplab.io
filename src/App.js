import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import "./css/stylesheet-small.css";
import "./css/stylesheet.css";

import Home from "./pages/Home";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import SignUp from "./pages/SignUp";
import Preferences from "./pages/Preferences";

function App() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" component={Home} exact />
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/login" component={Login} />
                <Route path="/signup" component={SignUp} />
                <Route path="/preferences" component={Preferences} />
                <Redirect to="/" />
            </Switch>
        </BrowserRouter>
    );
}

export default App;
