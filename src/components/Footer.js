import React, { Component } from "react";
import HighlightLink from "./HighlightLink";

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <HighlightLink
                    id="personal-website-link"
                    link="https://mljustinli.gitlab.io"
                    newTab="true"
                >
                    Justin Li
                    <br />© 2020
                </HighlightLink>
            </div>
        );
    }
}

export default Footer;
