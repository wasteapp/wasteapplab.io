import React, { Component } from "react";

/**
 * props: on, smaller, onClick
 */
class ThemeButtonToggle extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick(event) {
        event.preventDefault();
        this.props.onClick(this.props.children, !this.props.on);
    }

    render() {
        return (
            <button
                className={
                    "theme-button " +
                    (this.props.on ? "" : "theme-button-unselected") +
                    " " +
                    (this.props.smaller ? "theme-button-in-row" : "")
                }
                onClick={this.onClick}
            >
                {this.props.children}
            </button>
        );
    }
}

export default ThemeButtonToggle;
