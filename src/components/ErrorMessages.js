module.exports = {
    // General
    SERVER_CONNECT_FAILED: "Cannot connect to the server right now.",

    // sign up
    NO_FIRST_NAME: "Please enter your first name.",
    NO_LAST_NAME: "Please enter your last name.",
    INVALID_EMAIL: "Please enter a valid email.",
    NO_PASSWORD: "Please enter a password",
    UNSECURE_PASSWORD:
        "Please make sure your password has 3 numbers, one uppercase letter, and a symbol.",

    // login
    LOGIN_WITHOUT_PASSWORD: "Please enter your password.",
};
