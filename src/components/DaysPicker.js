import React, { Component } from "react";

/**
 * Number of Days Picker
 *
 * Requires
 * props: days, maxDays, onChange
 */
class DaysPicker extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.props.onChange(event.target.value);
    }

    render() {
        let days = [...Array(this.props.maxDays).keys()].map((i) => (
            <option value={i + 1} key={i}>
                {i + 1} {i + 1 === 1 ? "day" : "days"}
            </option>
        ));

        return (
            <select
                name="days"
                className="days-picker"
                value={this.props.days}
                onChange={this.handleChange}
            >
                {days}
            </select>
        );
    }
}

export default DaysPicker;
