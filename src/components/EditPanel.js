import React, { Component } from "react";
import ThemeButtonRow from "../components/ThemeButtonRow";
import moment from "moment";
import Calendar from "../components/Calendar";

import {
    DATE_FORMAT,
    DEFAULT_DAYS_UNTIL_EXPIRATION,
} from "../components/Constants";
import WeekDaySelect from "./WeekDaySelect";
import InfoButton from "./InfoButton";

/**
 * Panel used to add or edit a food
 *
 * props: the whole food object
 * onChange
 * onCancel
 */
class EditPanel extends Component {
    constructor(props) {
        super(props);

        let editFood;
        let foodExists;

        // set state's editFood to the props edit food if one is given
        if (this.props.editFood) {
            editFood = this.props.editFood;
            foodExists = true;
        } else {
            editFood = {
                name: "",
                expDate: moment()
                    .add(DEFAULT_DAYS_UNTIL_EXPIRATION, "days")
                    .format(DATE_FORMAT),
                frequency: {
                    freq_type: "daily",
                    extra: moment().format(DATE_FORMAT),
                },
                meals: {
                    breakfast: true,
                    lunch: true,
                    dinner: true,
                },
            };
            foodExists = false;
        }

        console.log("Default days: " + DEFAULT_DAYS_UNTIL_EXPIRATION);
        console.log(
            "Exp date: " +
                moment()
                    .add(DEFAULT_DAYS_UNTIL_EXPIRATION, "days")
                    .format(DATE_FORMAT)
        );

        this.state = {
            editFood: editFood,
            foodExists: foodExists,
            errorMessage: "",
        };

        console.log("edit food!");
        console.log(this.state.editFood);

        this.keyWasPressed = this.keyWasPressed.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleExpDateChange = this.handleExpDateChange.bind(this);
        this.handleMealsChange = this.handleMealsChange.bind(this);
        this.handleFrequencyChange = this.handleFrequencyChange.bind(this);
        this.handleFrequencyExtraChange = this.handleFrequencyExtraChange.bind(
            this
        );
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        document.body.style.overflow = "hidden";

        document.addEventListener("keydown", this.keyWasPressed, false);
    }

    componentWillUnmount() {
        document.body.style.overflow = "scroll";

        document.removeEventListener("keydown", this.keyWasPressed, false);
    }

    keyWasPressed(event) {
        if (event.keyCode === 27) {
            this.props.onCancel();
        }
    }

    handleNameChange(event) {
        let editFood = this.state.editFood;
        editFood.name = event.target.value;
        this.setState({ editFood: editFood });
    }

    handleExpDateChange(newExpDate) {
        let editFood = this.state.editFood;
        editFood.expDate = newExpDate;
        this.setState({ editFood: editFood }, () => {
            console.table(this.state);
        });
    }

    handleMealsChange(newMeals) {
        // TODO finish hooking up edit/add food view to this.state
        // let editFood = this.state.editFood;
        // editFood.meals = newMeals;
        // this.setState({ editFood: editFood });
        let correctKeysMeals = {
            breakfast: newMeals.Break,
            lunch: newMeals.Lunch,
            dinner: newMeals.Dinner,
        };
        console.log(correctKeysMeals);

        this.setState({
            editFood: {
                ...this.state.editFood,
                meals: correctKeysMeals,
            },
        });
    }

    /**
     * Change the frequency based on the new one
     *
     * @param {Object} newFrequency booleans with Daily: bool, Weekly: bool, Once: bool
     */
    handleFrequencyChange(newFrequency) {
        // TODO finish hooking up edit/add food view to this.state
        let newType = newFrequency.Once
            ? "Once"
            : newFrequency.Daily
            ? "Daily"
            : "Weekly";
        newType = newType.toLowerCase();

        let newExtra = newFrequency["Weekly"]
            ? ""
            : moment().format(DATE_FORMAT);

        this.setState(
            {
                editFood: {
                    ...this.state.editFood,
                    frequency: {
                        ...this.state.editFood.frequency,
                        extra: newExtra,
                        freq_type: newType,
                    },
                },
            },
            () => {
                console.table(this.state);
                console.log(
                    "the extra: " + this.state.editFood.frequency.extra
                );
            }
        );
    }

    /**
     * Change the frequency extra info based on the new one
     *
     * @param {Object} extra extra information
     * once: which date
     * daily: which date to start
     * weekly: which day of the week
     */
    handleFrequencyExtraChange(extra) {
        console.log("the extra is: " + extra);
        this.setState(
            {
                editFood: {
                    ...this.state.editFood,
                    frequency: {
                        ...this.state.editFood.frequency,
                        extra: extra,
                    },
                },
            },
            () => {
                console.table(this.state);
            }
        );
    }

    handleSubmit(event) {
        event.preventDefault();
        console.table(this.state);

        this.setState({ errorMessage: "" });

        // Validate fields

        // Check that a food name is entered
        if (this.state.editFood.name === "") {
            return this.setState({ errorMessage: "Please enter a food name." });
        }

        console.log("extra is: " + this.state.editFood.frequency.extra);

        // Check that at least one day is selected for a weekly reminder
        if (
            this.state.editFood.frequency.freq_type === "weekly" &&
            this.state.editFood.frequency.extra === ""
        ) {
            return this.setState({
                errorMessage:
                    "Please choose at least one day for a weekly reminder.",
            });
        }

        // Check that at least one meal is selected
        if (
            Object.values(this.state.editFood.meals).reduce(
                (a, meal) => a + meal,
                0
            ) === 0
        ) {
            return this.setState({
                errorMessage:
                    "Please choose at least one meal for the reminder.",
            });
        }

        if (this.state.errorMessage === "") {
            this.props.onChange(this.state.editFood, this.state.foodExists);
        }
    }

    render() {
        return (
            <div className="edit-panel-wrapper">
                <div className="edit-panel-background">
                    <div className="edit-panel">
                        <form id="food-form" onSubmit={this.handleSubmit}>
                            <div className="edit-header-wrapper">
                                <div className="edit-header">
                                    Add a Food
                                    <InfoButton>
                                        Add a short name or description of your
                                        food or ingredient!
                                        <br />
                                        <br />
                                        For example: "Use the scallions that are
                                        almost expired!!!"
                                    </InfoButton>
                                </div>
                            </div>
                            <input
                                autoFocus
                                placeholder="food name"
                                type="text"
                                onChange={this.handleNameChange}
                                value={this.state.editFood.name}
                            />
                            <div className="edit-header">
                                Expires On
                                <InfoButton>
                                    What day does the food or ingredient expire?
                                </InfoButton>
                            </div>
                            <Calendar
                                onChange={this.handleExpDateChange}
                                defaultSelectedDate={
                                    this.state.editFood.expDate
                                }
                            />
                            <div className="edit-header">
                                Reminders
                                <InfoButton>
                                    When do you want to receive reminders?
                                    <br />
                                    <br />
                                    You can choose at which meals and how
                                    frequently you'll receive reminders.
                                </InfoButton>
                            </div>
                            <div className="edit-sub-header">Meals</div>
                            <ThemeButtonRow
                                nameList={["Break", "Lunch", "Dinner"]}
                                defaultOnList={{
                                    Break: this.state.editFood.meals.breakfast,
                                    Lunch: this.state.editFood.meals.lunch,
                                    Dinner: this.state.editFood.meals.dinner,
                                }}
                                onChange={this.handleMealsChange}
                            />
                            <div className="edit-sub-header">Frequency</div>
                            <ThemeButtonRow
                                onlyOne={true}
                                nameList={["Once", "Daily", "Weekly"]}
                                defaultOnList={{
                                    Once:
                                        this.state.editFood.frequency
                                            .freq_type === "once",
                                    Daily:
                                        this.state.editFood.frequency
                                            .freq_type === "daily",
                                    Weekly:
                                        this.state.editFood.frequency
                                            .freq_type === "weekly",
                                }}
                                onChange={this.handleFrequencyChange}
                            />
                            {/* TODO change edit header based on once, daily, or weekly */}
                            <div className="edit-header">
                                Reminder Date
                                <InfoButton>
                                    {this.state.editFood.frequency.freq_type ===
                                        "once" &&
                                        "On what day do you want to be reminded?"}
                                    {this.state.editFood.frequency.freq_type ===
                                        "daily" &&
                                        "On what day do you want daily reminders to start?"}
                                    {this.state.editFood.frequency.freq_type ===
                                        "weekly" &&
                                        "On which days of the week do you want to be reminded?"}
                                </InfoButton>
                            </div>
                            {this.state.editFood.frequency.freq_type ===
                                "once" ||
                            this.state.editFood.frequency.freq_type ===
                                "daily" ? (
                                <Calendar
                                    onChange={this.handleFrequencyExtraChange}
                                    defaultSelectedDate={
                                        this.state.editFood.frequency.extra
                                    }
                                />
                            ) : (
                                <WeekDaySelect
                                    onChange={this.handleFrequencyExtraChange}
                                    defaultSelectedDays={
                                        this.state.editFood.frequency.extra
                                    }
                                />
                            )}
                            <div className="error-message">
                                {this.state.errorMessage}
                            </div>
                            <div id="food-submit-wrapper">
                                <button
                                    className="theme-button theme-button-unselected"
                                    onClick={this.props.onCancel}
                                >
                                    Cancel
                                </button>
                                <input
                                    className="theme-button"
                                    id="food-submit"
                                    type="submit"
                                    value="Done"
                                />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditPanel;
