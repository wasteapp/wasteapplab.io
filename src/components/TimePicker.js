import React, { Component } from "react";

/**
 * Hour and Minute Time Selector
 *
 * Requires
 * props: time (hh:mm), onChange
 */
class TimePicker extends Component {
    constructor(props) {
        super(props);

        let time = this.props.time.split(":");

        this.state = {
            hour: time[0],
            minute: time[1],
        };

        this.handleHourChange = this.handleHourChange.bind(this);
        this.handleMinuteChange = this.handleMinuteChange.bind(this);
    }

    handleHourChange(event) {
        this.setState({ hour: event.target.value });
        this.props.onChange(event.target.value + ":" + this.state.minute);
    }

    handleMinuteChange(event) {
        this.setState({ minute: event.target.value });
        this.props.onChange(
            this.state.hour + ":" + event.target.value.padStart(2, "0")
        );
    }

    render() {
        let hours = [...Array(24).keys()].map((i) => (
            <option value={i + 1} key={i}>
                {i + 1}
            </option>
        ));

        let minuteOptions = [0, 30];
        let minutes = minuteOptions.map((m) => (
            <option value={m} key={m}>
                {m.toString().padStart(2, "0")}
            </option>
        ));

        return (
            <div className="time-picker">
                <select
                    name="hour"
                    className="hour-picker"
                    onChange={this.handleHourChange}
                    value={this.state.hour}
                >
                    {hours}
                </select>
                <select
                    name="minute"
                    className="minute-picker"
                    onChange={this.handleMinuteChange}
                    value={this.state.minute}
                >
                    {minutes}
                </select>
            </div>
        );
    }
}

export default TimePicker;
