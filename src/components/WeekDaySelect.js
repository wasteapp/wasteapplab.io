import React, { Component } from "react";
import moment from "moment";

let weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
];

/**
 * props
 * {String} defaultSelectedDays the extra String when it's a Weekly frequency
 */
class WeekDaySelect extends Component {
    constructor(props) {
        super(props);

        // let selectedDays = [];

        // [...Array(7).keys()].map(
        //     (i) => (selectedDays[i] = i === moment().day())
        // );

        // this.state = {
        //     selectedDays: selectedDays,
        // };

        this.onClick = this.onClick.bind(this);
    }

    onClick(index) {
        let dayToToggle = weekdays[index];
        let selectedDays = this.props.defaultSelectedDays;

        if (selectedDays.includes(dayToToggle)) {
            selectedDays = selectedDays.replace("," + dayToToggle, "");
            selectedDays = selectedDays.replace(dayToToggle, "");
            if (selectedDays.charAt(0) === ",") {
                selectedDays = selectedDays.replace(",", "");
            }
        } else {
            // messes up order but... hmm
            selectedDays += (selectedDays === "" ? "" : ",") + dayToToggle;
        }

        console.log(selectedDays);
        this.props.onChange(selectedDays);
    }

    render() {
        console.log("default in render: " + this.props.defaultSelectedDays);

        let selectedDays = [];
        [...Array(7).keys()].map(
            (i) => (selectedDays[i] = i === moment().day())
        );
        let splitDays = this.props.defaultSelectedDays.split(",");
        splitDays.forEach((day) => {
            selectedDays[day.trim()] = true;
        });

        let elements = weekdays.map((day, index) => (
            <div className="day-wrapper" key={index}>
                <div
                    className={
                        "day " + (selectedDays[day] ? "day-selected " : "")
                    }
                    onClick={() => {
                        this.onClick(index);
                    }}
                >
                    {day.charAt(0)}
                </div>
            </div>
        ));

        // TODO create the weekday selector owo
        return (
            <div className="weekday-selector">
                <div className="day-nav">{elements}</div>
            </div>
        );
    }
}

export default WeekDaySelect;
