import React, { Component } from "react";
import { ReactComponent as Info } from "../images/info.svg";

class InfoButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showInfo: false,
        };

        this.onClickOutside = this.onClickOutside.bind(this);

        this.onClick = this.onClick.bind(this);
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.onClickOutside, false);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.onClickOutside, false);
    }

    onClickOutside(event) {
        if (this.node.contains(event.target)) {
            console.log("clicked outside!");
            return;
        }

        if (this.state.showInfo) {
            this.onClick();
        }
    }

    onClick() {
        this.setState({
            showInfo: !this.state.showInfo,
        });
    }

    render() {
        return (
            <div className="info-button" ref={(node) => (this.node = node)}>
                <Info onClick={this.onClick} />
                <div className="info-button-content-wrapper">
                    {this.state.showInfo && (
                        <div className="info-button-content">
                            {this.props.children}
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default InfoButton;
