import React, { Component } from "react";

class Highlight extends Component {
    render() {
        return (
            <span className="highlight" id={this.props.id}>
                {this.props.children}
            </span>
        );
    }
}

export default Highlight;
