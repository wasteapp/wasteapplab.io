import React, { Component } from "react";

class AboutSection extends Component {
    render() {
        return (
            <div className="about-wrapper">
                <div className="about-title">{this.props.title}</div>
                <div className="about-content">{this.props.children}</div>
            </div>
        );
    }
}

export default AboutSection;
