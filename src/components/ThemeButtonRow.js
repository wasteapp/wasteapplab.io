import React, { Component } from "react";
import ThemeButtonToggle from "./ThemeButtonToggle";

/**
 * props:
 * nameList (list of button names, all unique)
 * onlyOne (boolean if only one can be selected)
 * defaultOnList (in order, which are on) {key: boolean on or off}
 */
class ThemeButtonRow extends Component {
    constructor(props) {
        super(props);

        let onList = {};
        this.props.nameList.forEach((name) => (onList[name] = false));

        console.log(this.props.defaultOnList);

        this.state = {
            onList: this.props.defaultOnList,
        };

        console.log(this.state);
        this.updateList = this.updateList.bind(this);
    }

    updateList(name, val) {
        if (this.props.onlyOne) {
            console.log(name);
            let newOnList = {};
            this.props.nameList.forEach((n) => {
                if (n === name) {
                    newOnList[n] = true;
                } else {
                    newOnList[n] = false;
                }
            });

            this.setState({ onList: newOnList }, () =>
                this.props.onChange(this.state.onList)
            );
        } else {
            this.setState(
                (prevState) => ({
                    onList: {
                        ...prevState.onList,
                        [name]: val,
                    },
                }),
                () => this.props.onChange(this.state.onList)
            );
        }
    }

    render() {
        let buttons = this.props.nameList.map((name) => (
            <ThemeButtonToggle
                on={this.state.onList[name]}
                smaller={true}
                key={name}
                onClick={this.updateList}
            >
                {name}
            </ThemeButtonToggle>
        ));
        return <div className="theme-buttons-row">{buttons}</div>;
    }
}

export default ThemeButtonRow;
