import { TOKEN_KEY, USER_ID_KEY } from "./Constants";

export default function isLoggedInLocally() {
    let token = localStorage.getItem(TOKEN_KEY);
    let user_id = localStorage.getItem(USER_ID_KEY);

    return token != null && user_id != null;
}

export function logOut() {
    localStorage.clear();
}
