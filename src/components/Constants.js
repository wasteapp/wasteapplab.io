module.exports = {
    SERVER: "//waste-backend.herokuapp.com",
    CREATE_USER: "/user/createuser",
    LOGIN_USER: "/user/login",

    GET_PREFS: "/user/getprefs",
    UPDATE_PREFS: "/user/updateprefs",

    GET_FOODS: "/user/getfoods",
    REMOVE_FOOD: "/user/removefood",
    ADD_FOOD: "/user/addfood",
    UPDATE_FOOD: "/user/updatefood",

    TOKEN_KEY: "token",
    USER_ID_KEY: "user_id",

    DATE_FORMAT: "YYYY-MM-DD",
    DEFAULT_DAYS_UNTIL_EXPIRATION: 7,
};
