import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class HighlightLink extends Component {
    render() {
        if (this.props.link.includes("http")) {
            return (
                <span className="highlight-link" id={this.props.id}>
                    <a
                        href={this.props.link}
                        id={this.props.id + "-a"}
                        target={this.props.newTab ? "_blank" : ""}
                        rel="noopener noreferrer"
                    >
                        {this.props.children}
                    </a>
                </span>
            );
        }

        return (
            <span className="highlight-link" id={this.props.id}>
                <NavLink to={this.props.link}>{this.props.children}</NavLink>
            </span>
        );
    }
}

export default HighlightLink;
