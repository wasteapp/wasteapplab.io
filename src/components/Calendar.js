import React, { Component } from "react";
import moment from "moment";

import { ReactComponent as Left } from "../images/chevron-left.svg";
import { ReactComponent as Right } from "../images/chevron-right.svg";
import { DATE_FORMAT, DEFAULT_DAYS_UNTIL_EXPIRATION } from "./Constants";

/**
 * props
 * onChange
 * defaultSelectedDate (in DATE_FORMAT)
 */
class Calendar extends Component {
    constructor(props) {
        super(props);

        console.log("Default selected date: " + this.props.defaultSelectedDate);

        let selectedDate = this.props.defaultSelectedDate
            ? moment(this.props.defaultSelectedDate)
            : moment().add(DEFAULT_DAYS_UNTIL_EXPIRATION, "days");
        this.state = {
            selectedDate: selectedDate,
            viewingDate: selectedDate,
        };

        console.log("State is now : ");
        console.table(this.state);

        this.goToNextMonth = this.goToNextMonth.bind(this);
        this.goToPreviousMonth = this.goToPreviousMonth.bind(this);
    }

    getNumberDaysPadding() {
        let viewingDate = this.state.viewingDate;
        let firstDayOfMonthDate = moment([
            viewingDate.year(),
            viewingDate.month(),
            1,
        ]);

        console.log(firstDayOfMonthDate.format(DATE_FORMAT));
        console.log(firstDayOfMonthDate.weekday());

        return firstDayOfMonthDate.weekday();
    }

    /**
     * Handles when a day is clicked
     *
     * @param {Number} dayOfMonth 1 indexed
     */
    dayClicked(dayOfMonth) {
        let newDate = moment([
            this.state.viewingDate.year(),
            this.state.viewingDate.month(),
            dayOfMonth,
        ]);

        this.setState(
            {
                selectedDate: newDate,
            },
            () => {
                this.props.onChange(
                    this.state.selectedDate.format(DATE_FORMAT)
                );
                console.log(this.state.selectedDate.format(DATE_FORMAT));
            }
        );
    }

    /**
     * Handles month navigation
     */
    goToNextMonth() {
        let newDate = moment([
            this.state.viewingDate.year(),
            this.state.viewingDate.month(),
            1,
        ]);
        newDate = newDate.add(1, "month");

        this.setState(
            {
                viewingDate: newDate,
            },
            () => {
                console.log(this.state.viewingDate.format(DATE_FORMAT));
            }
        );
    }

    /**
     * Handles month navigation
     */
    goToPreviousMonth() {
        let newDate = moment([
            this.state.viewingDate.year(),
            this.state.viewingDate.month(),
            1,
        ]);
        newDate = newDate.subtract(1, "month");

        this.setState(
            {
                viewingDate: newDate,
            },
            () => {
                console.log(this.state.viewingDate.format(DATE_FORMAT));
            }
        );
    }

    render() {
        let daysInMonth = this.state.viewingDate.daysInMonth();
        let showingCurrentMonth =
            moment().month() === this.state.viewingDate.month() &&
            moment().year() === this.state.viewingDate.year();
        let showingSelectedMonth =
            this.state.viewingDate.month() ===
                this.state.selectedDate.month() &&
            this.state.viewingDate.year() === this.state.selectedDate.year();

        let days = [...Array(daysInMonth).keys()].map((i) => (
            <div className="day-wrapper" key={i}>
                <div
                    className={
                        "day " +
                        (showingCurrentMonth && i + 1 === moment().date()
                            ? "day-current "
                            : "") +
                        (showingSelectedMonth &&
                        i + 1 === this.state.selectedDate.date()
                            ? "day-selected "
                            : "")
                    }
                    onClick={() => this.dayClicked(i + 1)}
                >
                    {i + 1}
                </div>
            </div>
        ));

        let daysPadding = this.getNumberDaysPadding();

        [...Array(daysPadding).keys()].map((i) =>
            days.unshift(
                <div className="day-wrapper" key={"padding" + i}></div>
            )
        );

        return (
            <div className="calendar">
                <div className="month-nav">
                    <div
                        className="month-arrow previous-month"
                        onClick={this.goToPreviousMonth}
                    >
                        <Left />
                    </div>
                    <div className="month">
                        {this.state.viewingDate.format("MMMM")}{" "}
                        {this.state.viewingDate.year()}
                    </div>
                    <div
                        className="month-arrow next-month"
                        onClick={this.goToNextMonth}
                    >
                        <Right />
                    </div>
                </div>
                <div className="day-nav">{days}</div>
            </div>
        );
    }
}

export default Calendar;
