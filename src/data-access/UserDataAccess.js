import axios from "axios";
import { SERVER, CREATE_USER, LOGIN_USER } from "../components/Constants";
import moment from "moment-timezone";
const {
    NO_FIRST_NAME,
    NO_LAST_NAME,
    INVALID_EMAIL,
    UNSECURE_PASSWORD,
    SERVER_CONNECT_FAILED,
    LOGIN_WITHOUT_PASSWORD,
} = require("../components/ErrorMessages");

/**
 * API Calls
 *
 */

/**
 * Creates a user if the fields are valid.
 *
 * @param {String} firstName
 * @param {String} lastName
 * @param {String} email
 * @param {String} key
 * @returns {success: false, message: String} or
 *          the response from a successful api call
 */
export async function createUser(firstName, lastName, email, key) {
    if (!isFirstNameValid(firstName)) {
        return { success: false, message: NO_FIRST_NAME };
    }
    if (!isLastNameValid(lastName)) {
        return { success: false, message: NO_LAST_NAME };
    }
    if (!isEmailValid(email)) {
        return { success: false, message: INVALID_EMAIL };
    }
    if (!isKeyValid(key)) {
        return { success: false, message: UNSECURE_PASSWORD };
    }

    // make the api request
    let response = await requestAxios(CREATE_USER, {
        user_information: {
            firstName: firstName,
            lastName: lastName,
            email: email,
            key: key,
        },
        timezone: moment.tz.guess(true),
    });

    return response;
}

/**
 * Verify the user's credentials.
 *
 * @param {String} email
 * @param {String} key
 * @returns {success: false, message: String} or
 *          the response from a successful api call
 */
export async function loginUser(email, key) {
    if (!isEmailValid(email)) {
        return { success: false, message: INVALID_EMAIL };
    }
    if (!isKeyValid(key)) {
        return { success: false, message: LOGIN_WITHOUT_PASSWORD };
    }

    let response = await requestAxios(LOGIN_USER, {
        email: email,
        key: key,
    });
    return response;
}

/**
 * Makes an API call post
 *
 * @param {String} route
 * @param {Object} data can be null/not given
 * @return {success: Boolean, message: String} or a successful response object
 */
export async function makeAPICall(route, data) {
    let loginAttempt = checkLoggedIn();
    if (!loginAttempt.success) {
        return loginAttempt;
    }

    let newData = data == null ? {} : data;
    newData.user_id = loginAttempt.user_id;
    newData.user_token = loginAttempt.token;

    // make the api request
    let response = await requestAxios(route, newData);
    return response;
}

/**
 * Requests data from a given route
 *
 * @param {String} route
 * @param {Object} data can be null/not given
 * @return {success: Boolean, message: String} or a successful response object
 */
async function requestAxios(route, data) {
    let response = await axios
        .post(SERVER + route, data, { crossdomain: true, crossorigin: true })
        .catch((err) => {
            if (
                err.response &&
                err.response.data &&
                err.response.data.message
            ) {
                return { success: false, message: err.response.data.message };
            } else {
                return { success: false, message: SERVER_CONNECT_FAILED };
            }
        });
    return response;
}

/**
 * Check if the user is already logged in/there's an existing token and ID
 */
function checkLoggedIn() {
    let token = localStorage.getItem("token");
    let user_id = localStorage.getItem("user_id");

    if (token == null || user_id == null) {
        console.log("Token and or user id are null.");
        console.log("Please log in again.");

        // TODO show some login message or redirect
        return {
            success: false,
            message: "Session has expired. Please log in again.",
        };
    }
    return {
        success: true,
        token: token,
        user_id: user_id,
    };
}

/**
 * Helper Functions
 */

/**
 * Checks if the first name is valid
 *
 * @param {String} firstName
 */
function isFirstNameValid(firstName) {
    if (firstName == null) {
        return false;
    }

    if (firstName === "") {
        return false;
    }

    return true;
}

/**
 * Checks if the last name is valid
 *
 * @param {String} lastName
 */
function isLastNameValid(lastName) {
    if (lastName == null) {
        return false;
    }

    if (lastName === "") {
        return false;
    }

    return true;
}

/**
 * Checks if the email is valid
 *
 * @param {String} email
 */
function isEmailValid(email) {
    if (!email) {
        return false;
    }

    // https://flaviocopes.com/how-to-validate-email-address-javascript/
    const emailRegex = /\S+@\S+/;
    return emailRegex.test(String(email).toLowerCase());
}

/**
 * Checks if the user's key is valid
 *
 * @param {String} key
 */
function isKeyValid(key) {
    if (!key) {
        return false;
    }

    if (key === "") {
        return false;
    }

    return true;
}
